const express = require("express");
const router = express.Router();

const UserController = require("../controllers/userControllers");

//Route for checking if the user's email already exists in the database

router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result));
});


//Registration for user
//http://localhost:4000/api/users/register
router.post("/register", (req, res) =>{
	UserController.registerUser(req.body).then(result => res.send(result));
})





module.exports = router;