const jwt = require("jsonwebtoken");
const secret = "CrushAkoNgCrushKo";

//JSON Web Token or JWT is a way of securely passing information from the server to the front end or to other parts of server
//Information is kept secure through the use of secret code
//Only the system that knows the secret code that can decode the encrypted information


//Token Creation
/*
- Analogy
	Pack the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user) => {
	//The data will be received from the registration form
	//When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};


	//Generate a JSON web token using the jwt's method (sign())

	return jwt.sign(data, secret, {})
}














