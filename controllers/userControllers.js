const User = require("../models/User");
//encrypted password
const bcrypt = require('bcrypt');

//Check if the email already exists
/*
1. use mongoose "find" method to find duplicate emails
2. use the then method to send a response back to the client

*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		//if match is found
		if(result.length > 0){
			return true;
		} else {
			//No duplicate email found
			//The user is not yet registered in the database
			return false;
		}
	})
}



//User Registration
/*
Steps:
1. Create a new User object 
2. Make sure that the password is encrypted
3. Save the new User to the database
*/
/*
Function parameters from routes to controllers

Routes (argument)			Controllers(parameter)
registerUser(req.body, req.params) = >  (reqBody, params)
req.body.firstName => reqBody.firstName
*/


module.exports.registerUser = (reqBody) => {

	//Creates a new User Object
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//Saves the created object to our database
	return newUser.save().then((user, error) => {
		//User registration failed
		if(error) {
			return false;
		} else {
			//User Registration is success
			return true
		}
	})

}


















